
<h3>Add News</h3>
  <a href="<?php echo base_url('news/index'); ?>" class="btn btn-dfault">Back</a>
  <form action="<?php echo base_url('news/submit') ?>" method="post" class="form-horizontal">
    <div class="form-group">
      <label for="title" class="col-md-2 text-right">Title</label>
      <div class="col-md-10">
        <input type="text" name="title" class="form-control" required />
      </div>
    </div>
    <div class="form-group">
      <label for="text" class="col-md-2 text-right">Text</label>
      <div class="col-md-10">
        <textarea name="text" class="form-control" required></textarea>
      </div>
    </div>
    <div class="form-group">
      <label for="text" class="col-md-2 text-center"></label>
      <div class="col-md-10">
        <input type="submit" name="btnSave" class="btn btn-primary" value="Save"/>
      </div>
    </div>
  </form>
