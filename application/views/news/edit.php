
<h3>Edit News</h3>
  <a href="<?php echo base_url('news/index'); ?>" class="btn btn-dfault">Back</a>
  <form action="<?php echo base_url('news/update') ?>" method="post" class="form-horizontal">
    <input type="hidden" name="hidden_id" value="<?php echo $actual_news->id;?>">
    <div class="form-group">
      <label for="title" class="col-md-2 text-right">Title</label>
      <div class="col-md-10">
        <input type="text" value="<?php echo $actual_news->title;?>" name="title" class="form-control" required />
      </div>
    </div>
    <div class="form-group">
      <label for="text" class="col-md-2 text-right">Text</label>
      <div class="col-md-10">
        <textarea name="text" class="form-control" required><?php echo $actual_news->text;?></textarea>
      </div>
    </div>
    <div class="form-group">
      <label for="text" class="col-md-2 text-center"></label>
      <div class="col-md-10">
        <input type="submit" name="btnUpdate" class="btn btn-primary" value="Update"/>
      </div>
    </div>
  </form>
