
      <h3>News list</h3>

      <?php
        if($this->session->flashdata('success_msg')){
      ?>
        <div class="alert alert-success">
          <?php echo $this->session->flashdata('success_msg');?>
        </div>
      <?php
        }
        else{
      ?>
      <div class="alert alert-error">
        <?php echo $this->session->flashdata('error_msg');?>
      </div>
      <?php
        }
      ?>

      <a href="<?php echo base_url('news/add'); ?>"
        class="btn btn-primary">Add new</a>
      <div style="overflow:auto">
        <table class="table table-bordered table-responsive" style="width: 100%;">
          <thead>
            <tr>
              <td>ID</td>
              <td>title</td>
              <td>action</td>
            </tr>
            <tbody>
              <?php
              if($news){
                foreach ($news as $actual_news) {
                  ?>
                  <tr>
                    <td width=><?php echo $actual_news->id;?></td>
                    <td><?php echo $actual_news->title;?></td>
                    <td>
                      <a href="<?php echo base_url('news/edit/'.$actual_news->id);?>"
                        class="btn btn-info">Edit</a>
                        <a href="<?php echo base_url('news/delete/'.$actual_news->id);?>"
                          class="btn btn-danger"
                          onclick="return confirm('Do you want to delete this record?')">
                          Delete
                        </a>
                      </td>
                    </tr>
                    <?php
                  }
                }
                ?>
              </tbody>
            </thead>
          </table>
      </div>
