<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class News_m extends CI_Model{
  public function getNews(){
    $query = $this->db->get('news');
    if($query->num_rows() > 0){
      return $query->result();
    }
    else{
      return false;
    }
  }

  public function submit(){
    $field = array(
      'id'=> md5(uniqid(rand(), true)),
      'title'=>$this->input->post('title'),
      'text'=>$this->input->post('text'),
    );
    $this->db->insert('news', $field);
    if($this->db->affected_rows() > 0){
      return true;
    }
    else{
      return false;
    }
  }

  public function getNewsById($id){
    $this->db->where('id', $id);
    $query = $this->db->get('news');
    if($query->num_rows() > 0){
      return $query->row();
    }
    else{
      return false;
    }
  }

  public function update(){
    $id = $this->input->post('hidden_id');
    $field = array(
      'id'=> md5(uniqid(rand(), true)),
      'title'=>$this->input->post('title'),
      'text'=>$this->input->post('text'),
    );
    $this->db->where('id', $id);
    $this->db->update('news', $field);
    if($this->db->affected_rows() > 0){
      return true;
    }
    else{
      return false;
    }
  }

  public function delete($id){
    $this->db->where('id', $id);
    $this->db->delete('news');
    if($this->db->affected_rows() > 0){
      return true;
    }
    else{
      return false;
    }
  }
}
