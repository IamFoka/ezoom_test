<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {

  function __construct(){
    parent:: __construct();
    $this->load->model('News_m', 'm');
  }

  function index(){
    $data['news'] = $this->m->getNews();
    $this->load->view('layout/header');
    $this->load->view('news/index', $data);
    $this->load->view('layout/footer');

  }

  public function add(){
    $this->load->view('layout/header');
    $this->load->view('news/add');
    $this->load->view('layout/footer');
  }

  public function submit(){
    $result = $this->m->submit();
    if($result){
      $this->session->set_flashdata('success_msg', 'Record added successfully');
    }
    else{
      $this->session->set_flashdata('error_msg', 'Fail to add record');
    }
    redirect(base_url('news/index'));
  }

  public function edit($id){
    $data['actual_news'] = $this->m->getNewsById($id);
    $this->load->view('layout/header');
    $this->load->view('news/edit', $data);
    $this->load->view('layout/footer');
  }

  public function update(){
    $result = $this->m->update();
    if($result){
      $_SESSION['success_msg'] = 'Record updated successfully';
      $this->session->mark_as_temp('success_msg', 5);
    }
    else{
      $this->session->set_flashdata('error_msg', 'Fail to update record');
    }
    redirect(base_url('news/index'));
  }

  public function delete($id){
    $result = $this->m->delete($id);
    if($result){
      $this->session->set_flashdata('success_msg', 'Record deleted successfully');
    }
    else{
      $this->session->set_flashdata('error_msg', 'Fail to delete record');
    }
    redirect(base_url('news/index'));
  }
}
