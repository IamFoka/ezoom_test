# ezoom_test

# Instalation
You'll need:   
MySQL    
MySQL Workbench (optional)     
PHP 7     
  
Then you'll need to create a table 'news' in your database, with this fields:
- id (varchar(32))  
- title (varchar(100))  
- text (mediumtext)  

After that, input your database data on /application/config/database  
- username    
- password    
- database    
    
# Running  
`$ php -S localhost:8080`
     
      
You can access on: [localhost:8080](localhost:8080)

